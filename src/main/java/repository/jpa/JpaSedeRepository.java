package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.SedeRepository;
import domain.Account;
import domain.Sede;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
		SedeRepository {

	@Override
	public Sede findByCodigo(double codigo) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM Sede a WHERE a.codigo = :codigo";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("codigo", codigo);
		return getFirstResult(query);
	}

	@Override
	public Collection<Account> findByPersonId(Long personId) {
		String jpaQuery = "SELECT a FROM Account a JOIN a.owners p WHERE p.id = :personId";
		TypedQuery<Account> query = entityManager.createQuery(jpaQuery, Account.class);
		query.setParameter("personId", personId);
		return query.getResultList();
	}

	
}
