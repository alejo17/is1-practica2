package repository;

import java.util.Collection;

import domain.Account;
import domain.Sede;

public interface SedeRepository extends BaseRepository<Sede, Long> {
	Sede findByCodigo(double codigo);

	Collection<Account> findByPersonId(Long personId);
}
