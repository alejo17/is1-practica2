package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_sedes", indexes = { @Index(columnList = "codigo") })
public class Sede implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "sede_id_generator", sequenceName = "sede_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sede_id_generator")
	private Long id;

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private double codigo;

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String direccion;

	@OneToMany(mappedBy = "cuentas_sede")
	private Collection<Account> cuentas;

	@OneToMany(mappedBy = "sourceAccount", fetch = FetchType.LAZY)
	private Collection<Operation> sourceOperations;

	@OneToMany(mappedBy = "targetAccount")
	private Collection<Operation> targetOperations;

	public Sede(double codigo, String direccion) {
		this.codigo = codigo;
		this.direccion = direccion;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public double getCodigo() {
		return codigo;
	}

	public void setCodigo(double codigo) {
		this.codigo = codigo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return "{codigo: " + codigo + ", direccion: " + direccion + "}";
	}

	public Collection<Account> getCuentas() {
		return cuentas;
	}

	public void setCuentas(Collection<Account> cuentas) {
		this.cuentas = cuentas;
	}

}
