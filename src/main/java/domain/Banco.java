package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Banco implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "banco_id_generator", sequenceName = "banco_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "banco_id_generator")
	private Long id;

	@Column(length = 64)
	private String Nombre;

	@OneToMany
    @JoinTable(name="banco_sedes")
	private Collection<Sede> sedes;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String Nombre) {
		this.Nombre = Nombre;
	}

	public Collection<Sede> getSedes() {
		return sedes;
	}

	public void setSedes(Collection<Sede> sedes) {
		this.sedes = sedes;
	}

}
